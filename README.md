# ENGLISH #

PuzzleSolver project for the course in Concurrent and Distributed Programming

University of Padua

Department of Mathematics

BSc in Computer Science

by Jacopo Cavallarin

## USAGE ##

To create puzzles you can use PuzzleCreator.java, which accepts both console and file input. Its output can then be used as input in the main program. It uses randomisation to mix the pieces of the puzzle.

There's already an example input, divina_in.txt which is the whole Divine Commedy by Dante Alighieri. It must be first converted to a puzzle using the aforementioned class (Be aware that the puzzle output is very large and might crash your text editor if opened).

The main program is split in 3 parts:

1. The first one is a simple, single-thread solver.

2. The second one is a concurrent variant of the first. It uses a variable number of threads depending on the size of the puzzle to solve.

3. The last part is a distributed variant of the previous. There's a server that solves puzzles and various clients that send puzzles to the server and receive the solutions.

To know more, please read the documentation for every part (specifica_parte#.pdf, Italian only).

# ITALIANO #

Progetto PuzzleSolver per il corso di Programmazione Concorrente e Distribuita

Università di Padova

Dipartimento di Matematica

Corso di Laurea in Informatica

Di Jacopo Cavallarin

## UTILIZZO ##

Per creare un puzzle da risolvere è possibile usare PuzzleCreator.java, che accetta input sia da console che da file.

E' già presente anche un input per PuzzleCreator, divina_in.txt, che contiene tutta la Divina Commedia di Dante Alighieri. Deve prima essere convertito in puzzle usando la classe sopracitata (attenzione che l'output è molto grande e potrebbe crashare il vostro editor di testo se aperto).

Il file di output di PuzzleCreator può poi essere usato come input in una qualsiasi delle tre parti del progetto.

Per informazioni su come utilizzare le tre versioni di PuzzleSolver, fare riferimento alle rispettive specifiche (specifica_parte#.pdf)