package io;

import java.io.*;
import java.util.Scanner;

public class FileManager {

    public static final String newLine = System.getProperty("line.separator");//Stringa di ritorno a capo utilizzata dal S.O. di esecuzione
    private static final String charset = "UTF-8";
    private static final String UTF8_BOM = "\uFEFF";//Byte Order Mark: Da rimuovere se presente come carattere iniziale nel file di input (Blocco Note di Windows lo inserisce di default)

    public static String readContent(String inputPath) {
        String file = "";
        try {
            Scanner scan = new Scanner(new FileInputStream(inputPath), charset).useDelimiter("\\Z");// "\\Z" è un'espressione regolare che indica la fine del file
            if (scan.hasNext())
                file = scan.next();//Copia tutto il contenuto del file nella stringa. Se il file è vuoto, la stringa rimane vuota.
            if (file.startsWith(UTF8_BOM))//Scarta il BOM, se presente
                file = file.substring(1);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return file;
    }

    public static void writeContent(String file, String content) {
        BufferedWriter writer;
        try {
            writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file), charset));
            writer.write(content);
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
