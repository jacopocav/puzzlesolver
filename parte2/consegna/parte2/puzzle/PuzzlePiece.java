package puzzle;

/*
Interfaccia che modella un tassello di un puzzle.
Ogni tassello ha un valore di un certo tipo, ottenibile tramite il metodo getVal()
Inoltre ha un proprio ID univoco di tipo String e deve fornire gli ID dei pezzi a lui adiacenti tramite i 4 metodi restanti
 */

public interface PuzzlePiece<A> {
    String getId();

    A getVal();

    String getNorth();//ID del vicino sopra

    String getEast();//ID del vicino a destra

    String getSouth();//ID del vicino sotto

    String getWest();//ID del vicino a sinistra

}
