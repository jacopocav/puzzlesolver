package puzzle;

import java.util.Map;

/*
Interfaccia che modella un parser di puzzle.
Il parser deve poter fornire una mappa in cui ci sono tutti i PuzzlePiece letti e riconosciuti da un input
Deve inoltre poter fornire il numero di colonne del puzzle e il PuzzlePiece dell'angolo superiore sinistro
 */

public interface PuzzleParser {
    Map<String, PuzzlePiece> getPuzzleMap();

    int getColCount();

    PuzzlePiece getAngle();
}
