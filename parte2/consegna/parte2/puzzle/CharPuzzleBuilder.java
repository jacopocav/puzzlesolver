package puzzle;

import io.FileManager;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class CharPuzzleBuilder implements PuzzleBuilder {

    private String matrixString, linearString;
    private List<List<PuzzlePiece>> solvedList;
    Map<String, PuzzlePiece> puzzleMap;

    
	private static final int minimumThreadNum = 1;//Numero minimo di thread per il TP
	private static final int logBase = 10;//Base del logaritmo per il calcolo del numero di thread ottimale
    
    public CharPuzzleBuilder(Map<String, PuzzlePiece> puzzleMap, PuzzlePiece angle) {

        this.puzzleMap = puzzleMap;
        solvedList = new ArrayList<List<PuzzlePiece>>();

        //Il numero di thread ottimale è il massimo tra il minimumThreadNum e il logaritmo in base logBase del numero di pezzi del puzzle
		int optimalThreadNum = Math.max(minimumThreadNum, (int) Math.round(Math.log(puzzleMap.size()) / Math.log(logBase)));

        //Inizializzazione del TP con il numero di thread calcolato
		final ThreadPoolExecutor exec = (ThreadPoolExecutor) Executors.newFixedThreadPool(optimalThreadNum);

        //Inserimento della prima riga
        solvedList.add(new ArrayList<PuzzlePiece>());
        solvedList.get(0).add(angle);

        //Riga da passare al task
        final List<PuzzlePiece> row = solvedList.get(0);
        //Inserimento del task nella coda tel TP
        exec.execute(new Runnable() {
            @Override
            public void run() { solveRow(row); }
        });

        //Riga di controllo del ciclo while
        List<PuzzlePiece> currentRow = solvedList.get(0);

        while(!currentRow.get(0).getSouth().equals(CharPuzzlePiece.emptyID)){//Finchè esiste una riga successiva a quella corrente

            solvedList.add(new ArrayList<PuzzlePiece>());

            List<PuzzlePiece> previousRow = currentRow;
            currentRow = solvedList.get(solvedList.size()-1);

            final List<PuzzlePiece> rowToSolve = currentRow;

            currentRow.add(puzzleMap.get(previousRow.get(0).getSouth()));

            exec.execute(new Runnable() {
                @Override
                public void run() { solveRow(rowToSolve); }
            });

        }

        exec.shutdown();//Richiesta di terminazione al TP: terminerà quando avrà finito tutti i task

        try {
            if (!exec.awaitTermination(60, TimeUnit.SECONDS)) {//Aspetto la terminazione del TP. Se ciò non avviene entro 60 secondi, ritorna false
                solvedList = null;
                matrixString = null;
                linearString = null;
                exec.shutdownNow();//Terminazione forzata del TP
                if (!exec.awaitTermination(60, TimeUnit.SECONDS)) {//Aspetto ancora che il TP termini entro 60 secondi
                    //Impossibile terminare il pool
                    throw new Exception("Impossibile terminare il pool");
                } else {
                    //Pool terminato prematuramente: tempo di timeout superato
                    throw new Exception("Pool terminato prematuramente: tempo di timeout superato");
                }
            }
        }catch(Exception ie){
            ie.printStackTrace();
        }

    }

    private void solveRow(List<PuzzlePiece> row){
        if(!row.isEmpty()){
            while(!row.get(row.size()-1).getEast().equals(CharPuzzlePiece.emptyID)){
                row.add(puzzleMap.get(row.get(row.size()-1).getEast()));
            }
        }
    }

    @Override
    public String getMatrixString() {

        if (matrixString == null && solvedList != null) {
            StringBuilder matrix = new StringBuilder("");

            for (List<PuzzlePiece> row : solvedList) {
                for (PuzzlePiece piece : row)
                    matrix.append(piece.getVal());

                matrix.append(FileManager.newLine);
            }
            matrixString = matrix.toString();
            if(linearString == null) linearString = matrixString.replaceAll(FileManager.newLine, "");
            return matrixString;
        } else {
            if(solvedList == null) return "";
            else return matrixString;
        }

    }

    @Override
    public String getLinearString() {

        if(linearString == null) getMatrixString();
        return linearString;

    }

    //Ritorna il puzzle risolto, reso immutabile
    @Override
    public List<List<PuzzlePiece>> getSolvedList() {
        return Collections.unmodifiableList(solvedList);
    }

}
