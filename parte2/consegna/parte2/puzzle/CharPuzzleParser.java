package puzzle;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

//Questa classe implementa l'interfaccia PuzzleParser per i CharPuzzlePiece

public class CharPuzzleParser implements PuzzleParser {
    private Map<String, PuzzlePiece> puzzleMap;
    private int colCount;
    private PuzzlePiece angle;

    //Il costruttore si occupa di inizializzare i campi e di fare il parsing

    public CharPuzzleParser(String input) {

        Scanner scan = new Scanner(input);
        puzzleMap = new HashMap<String, PuzzlePiece>();
        colCount = 0;
        try {
            int lineNum = 1;
            while (scan.hasNextLine()) {
                String line = scan.nextLine();
                String[] values = line.split("\t");//Separa la linea in pezzi considerando come separatore la tabulazione

                //Controlla che la linea sia scritta in modo corretto
                if (values[1].length() != 1)
                    throw new IOException("ERRORE IN LINEA " + lineNum + " NEL FILE DI INPUT: Il secondo elemento non è un carattere singolo");
                if (values.length < 6)
                    throw new IOException("ERRORE IN LINEA " + lineNum + " NEL FILE DI INPUT: Mancano uno o più campi");
                if (values.length > 6)
                    System.err.println("AVVISO: IN LINEA " + lineNum + " NEL FILE DI INPUT: Ci sono più di 6 campi. Quelli in eccesso verranno ignorati");
                if (puzzleMap.containsKey(values[0]))
                    throw new IOException("ERRORE IN LINEA " + lineNum + " NEL FILE DI INPUT: L'ID " + values[0] + " non è univoco");

                if (values[2].equals(CharPuzzlePiece.emptyID)) {//Il campo north è VUOTO
                    colCount++;//Conto il numero di colonne della tabella
                    if (values[5].equals(CharPuzzlePiece.emptyID))//Anche il campo west è VUOTO: il pezzo è l'angolo superiore sinistro
                        angle = new CharPuzzlePiece(values[0], values[1].charAt(0), values[2], values[3], values[4], values[5]);
                }
                puzzleMap.put(values[0], new CharPuzzlePiece(values[0], values[1].charAt(0), values[2], values[3], values[4], values[5]));
                lineNum++;
            }
        } catch (IOException ex) {
            ex.printStackTrace();
            System.exit(1);
        }
    }

    public Map<String, PuzzlePiece> getPuzzleMap() {
        return puzzleMap;
    }

    public int getColCount() {
        return colCount;
    }

    public PuzzlePiece getAngle() {
        return angle;
    }
}
