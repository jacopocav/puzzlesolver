package puzzle;

import io.FileManager;

import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.List;

public class PuzzleSolverClient {


    private static final int PORT = 1099,//Porta da assegnare al registro RMI (default = 1099)
            MAX_TRIES = 5,//Tentativi di riconnessione massimi da effettuare (nel caso di errori)
            TIMEOUT = 5000;//Tempo di attesa tra un tentativo e un altro (ms)

    public static void main(String[] args) throws InterruptedException {
        //Lettura percorsi del file di input e di output e nome server
        String inputPath = args[0], outputPath = args[1], serverName = args[2];

        //Lettura da file a stringa
        String inputContent = FileManager.readContent(inputPath);

        if (!inputContent.isEmpty()) {

            //Parsing della stringa
            PuzzleParser parser = new CharPuzzleParser(inputContent);

            List<List<PuzzlePiece>> solvedPuzzle = null;
            boolean success = false;

            int tries = 0;
            for (; tries < MAX_TRIES && !success; tries++) {
                try {
                    //Prende il registro RMI sulla porta PORT, se presente
                    Registry rmi = LocateRegistry.getRegistry(PORT);

                    //Cerca l'oggetto server nel registro RMI
                    PuzzleServer server = (PuzzleServer) rmi.lookup(serverName);

                    //Richiede la risoluzione del puzzle al server
                    solvedPuzzle = server.solvePuzzle(parser.getPuzzleMap(),parser.getAngle());

                    success = true;

                } catch (Exception e) {
                    success = false;
                    if (tries < MAX_TRIES - 1) {
                        System.out.println("Errore durante la comunicazione con il server. Nuovo tentativo fra " + (TIMEOUT / 1000) + " secondi");
                        Thread.sleep(TIMEOUT);
                    }
                }
            }
            if (!success) {
                System.out.println("Impossibile stabilire una connessione stabile con il server. Esecuzione fallita");
                System.exit(1);
            }
            if (tries > 1) System.out.println("Connessione con il server recuperata con successo");

            int rowCount = solvedPuzzle.size(),
                    colCount = parser.getPuzzleMap().size() / rowCount;

            String matrixString = getMatrixString(solvedPuzzle),
                    linearString = matrixString.replaceAll(FileManager.newLine, "");


            String outputString
                    = linearString + FileManager.newLine + FileManager.newLine + matrixString + FileManager.newLine + rowCount + " " + colCount;

            //Scrittura output su file
            FileManager.writeContent(outputPath, outputString);

        } else {

            //Se il file di input era vuoto, nel file di output scrive solo la dimensione del puzzle (0x0)
            FileManager.writeContent(outputPath, FileManager.newLine + FileManager.newLine + FileManager.newLine + FileManager.newLine + "0 0");//Le 4 FileManager.newLine servono per mantenere l'output coerente con i requisiti
        }
    }

    private static String getMatrixString(List<List<PuzzlePiece>> solvedPuzzle) {
        StringBuilder matrix = new StringBuilder("");

        for (List<PuzzlePiece> row : solvedPuzzle) {
            for (PuzzlePiece piece : row)
                matrix.append(piece.getVal());

            matrix.append(FileManager.newLine);
        }
        return matrix.toString();

    }
}