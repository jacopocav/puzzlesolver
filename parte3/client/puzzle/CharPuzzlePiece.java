package puzzle;

/*
Implementazione dell'interfaccia PuzzlePiece<Character>.
Aggiunge una costante pubblica emptyID, che indica l'ID di un vicino mancante (necessario per i bordi del puzzle, dove i tasselli mancano di 1 o più vicini)
 */

public class CharPuzzlePiece implements PuzzlePiece<Character> {
    public static final String emptyID = "VUOTO";
    private String id, north, east, south, west;
    private char val;

    public CharPuzzlePiece(String id, char val, String north, String east, String south, String west) {
        this.id = id;
        this.val = val;
        this.north = north;
        this.east = east;
        this.south = south;
        this.west = west;
    }


    public String getId() {
        return id;
    }

    public Character getVal() {
        return val;
    }

    public String getNorth() {
        return north;
    }

    public String getEast() {
        return east;
    }

    public String getSouth() {
        return south;
    }

    public String getWest() {
        return west;
    }
}