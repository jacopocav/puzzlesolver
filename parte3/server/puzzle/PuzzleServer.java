package puzzle;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.List;
import java.util.Map;

public interface PuzzleServer extends Remote {

    List<List<PuzzlePiece>> solvePuzzle(Map<String,PuzzlePiece> map, PuzzlePiece angle) throws RemoteException;

}
