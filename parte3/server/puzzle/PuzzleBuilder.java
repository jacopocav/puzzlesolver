package puzzle;

import java.util.List;

public interface PuzzleBuilder {

    //Ritorna il puzzle in formato stringa lineare
    String getLinearString();

    //Ritorna il puzzle in formato stringa tabellare
    String getMatrixString();

    //Ritorna il puzzle risolto
    List<List<PuzzlePiece>> getSolvedList();
}
