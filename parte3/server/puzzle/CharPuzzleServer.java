package puzzle;


import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.List;
import java.util.Map;

public class CharPuzzleServer extends UnicastRemoteObject implements PuzzleServer {

    public CharPuzzleServer() throws RemoteException {}

    public List<List<PuzzlePiece>> solvePuzzle(Map<String,PuzzlePiece> map, PuzzlePiece angle) throws RemoteException {

        return new CharPuzzleBuilder(map,angle).getSolvedList();

    }

}
