package puzzle;

import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

public class PuzzleSolverServer {

    private static final int PORT = 1099,//Porta da assegnare al registro RMI (default = 1099)
            MAX_TRIES = 5,//Tentativi massimi da effettuare (nel caso di errori)
            TIMEOUT = 5000;//Tempo di attesa tra un tentativo e un altro (ms)

    public static void main(String[] args) throws InterruptedException {

        String serverName = args[0];

        boolean success = false;
        int tries = 0;
        for(; tries < MAX_TRIES && !success; tries++) {
            try {
                //Creazione oggetto PuzzleServer
                PuzzleServer server = new CharPuzzleServer();

                //Creazione registro RMI
                Registry rmi = LocateRegistry.createRegistry(PORT);

                //Rebind di server sul registro RMI
                rmi.rebind(serverName, server);

                success = true;
            }catch(RemoteException re){
                success = false;
                if (tries < MAX_TRIES - 1) {
                    System.out.println("Errore durante la preparazione del server. Nuovo tentativo fra " + (TIMEOUT / 1000) + " secondi");
                    Thread.sleep(TIMEOUT);
                }
            }
        }
        if(!success){
            System.out.println("Numero massimo di tentativi raggiunto");
            System.out.println("Impossibile preparare il server. Esecuzione fallita");
            System.exit(1);
        }
        if(tries > 1) System.out.println("Server preparato con successo");

        System.out.println("Server pronto. In attesa di client...");

    }


}