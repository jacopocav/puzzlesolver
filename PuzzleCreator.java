import java.io.*;
import java.util.ArrayList;
import java.util.Collections;

public class PuzzleCreator {
	
	private static final String charset = "UTF-8";
	private static final String UTF8_BOM = "\uFEFF";
	
	public static void main(String[] args) {
		
		char answer = '\0';
		while(answer != 'f' && answer != 't' && answer != 'q'){
			System.out.println("Scegli come inserire i dati (f = file, t = terminale, q = esci)");
			answer = System.console().readLine().charAt(0);
		}
		
		if(answer == 'q') System.exit(0);
		
		if(answer == 't' || answer == 'f'){
			int rows = -1, cols = -1;
			String input = null;
			if(answer == 't'){
				System.out.println("Inserire il numero di righe del puzzle:");
				rows = Integer.parseInt(System.console().readLine());
				System.out.println("Inserire il numero di colonne del puzzle:");
				cols = Integer.parseInt(System.console().readLine());
				System.out.println("Scrivere una frase di esattamente "+rows*cols+" caratteri (inclusi gli spazi, tabulazioni ignorate):");
				input = System.console().readLine();
				input.replaceAll("\t", "");
			} else {
				System.out.println("Scrivere il percorso del file:");
				String in = System.console().readLine();
				System.out.println("Path di Input: "+in);
				try {
				    	BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(in), charset));
					String firstLine = reader.readLine();
					if(firstLine.startsWith(UTF8_BOM)) firstLine = firstLine.substring(1);
					rows = Integer.parseInt(firstLine);
					cols = Integer.parseInt(reader.readLine());
					input = reader.readLine().replaceAll("\t", " ");
					reader.close();				
				} catch (IOException e) {
					System.err.println(e);
				}
			}
			
			System.out.println("Inserire il percorso del file di output:");
			String outPath = System.console().readLine();
			System.out.println("Path di output: "+outPath);
			if(input.length() < rows*cols) {
				while(input.length() < rows*cols) input = input+".";
			} else if(input.length() > rows*cols) input = input.substring(0, rows*cols-1);
			
			SimplePuzzlePiece[][] puzzle = new SimplePuzzlePiece[rows][cols];
			
			ArrayList<Integer> numbers = new ArrayList<Integer>();
			for(int i=0;i<rows*cols;++i) numbers.add(i, i);
			
			Collections.shuffle(numbers);
			
			for(int i=0; i<rows*cols;++i){
				puzzle[i/cols][i%cols] = new SimplePuzzlePiece(input.charAt(i),numbers.get(i).toString());
			}
			
			ArrayList<PuzzlePieceIn> boh = new ArrayList<PuzzlePieceIn>();
			
			for(int i=0; i<rows; ++i){
				for(int j=0; j<cols; ++j){
					
					String north,east,south,west;
					if(i>0) north = puzzle[i-1][j].getId();
					else north = "VUOTO";
					
					if(j<cols-1) east = puzzle[i][j+1].getId();
					else east = "VUOTO";
					
					if(i<rows-1) south = puzzle[i+1][j].getId();
					else south = "VUOTO";
					
					if(j>0) west = puzzle[i][j-1].getId();
					else west = "VUOTO";
					
					boh.add(new PuzzlePieceIn(puzzle[i][j],north,east,south,west));
				}	
				
			}
			Collections.shuffle(boh);
			
			StringBuilder output = new StringBuilder();
			for(PuzzlePieceIn pp:boh)	output.append(pp.getId()+"\t"+pp.getVal()+"\t"+pp.getNorth()+"\t"+pp.getEast()+"\t"+pp.getSouth()+"\t"+pp.getWest()+System.getProperty("line.separator"));
			
			writeContent(output.toString(), outPath);
		}
	}
		
	
	private static void writeContent(String content, String file) {
        	BufferedWriter writer;
	        try {
	            writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file), charset));
	            writer.write(content);
            	writer.close();
	        } catch (IOException e) {
	            e.printStackTrace();
	        }
	}

}

class SimplePuzzlePiece {
	private char val;
	private String id;
	
	public char getVal() {
		return val;
	}

	public String getId() {
		return id;
	}
	
	SimplePuzzlePiece(char v, String i){
		val = v;
		id = i;
	}

}

class PuzzlePieceIn {
	private char val;
	private String id, north, east, south, west;
	
	public char getVal() {
		return val;
	}
	public String getId() {
		return id;
	}
	public String getNorth() {
		return north;
	}
	public String getEast() {
		return east;
	}
	public String getSouth() {
		return south;
	}
	public String getWest() {
		return west;
	}
	
	PuzzlePieceIn(SimplePuzzlePiece sp, String n, String e, String s, String w){
		val = sp.getVal();
		id = sp.getId();
		north = n;
		east = e;
		south = s;
		west = w;
	}
}
