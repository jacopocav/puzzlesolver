package puzzle;

import io.FileManager;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

public class PuzzleBuilder {

    private StringBuilder linearString, matrixString;
    private List<List<PuzzlePiece>> solvedList;

    public PuzzleBuilder(Map<String, PuzzlePiece> puzzleMap, PuzzlePiece angle, String emptyID) {

        solvedList = new ArrayList<List<PuzzlePiece>>();

        double beginTime = System.nanoTime()/1000000;

        solvedList.add(new ArrayList<PuzzlePiece>());
        solvedList.get(0).add(angle);

        matrixString = new StringBuilder();
        linearString = new StringBuilder();

        matrixString.append(solvedList.get(0).get(0).getVal());
        linearString.append(solvedList.get(0).get(0).getVal());

        int row = 0;
        boolean done = false;

        while (!done) {
            int col = 0;
            while (!solvedList.get(row).get(col).getEast().equals(emptyID)) {
                solvedList.get(row).add(puzzleMap.get(solvedList.get(row).get(col).getEast()));
                col++;
                matrixString.append(solvedList.get(row).get(col).getVal());
                linearString.append(solvedList.get(row).get(col).getVal());
            }
            matrixString.append(FileManager.newLine);
            if (solvedList.get(row).get(0).getSouth().equals(emptyID)) done = true;
            else {
                solvedList.add(new ArrayList<PuzzlePiece>());
                solvedList.get(row + 1).add(puzzleMap.get(solvedList.get(row).get(0).getSouth()));
                row++;
                matrixString.append(solvedList.get(row).get(0).getVal());
                linearString.append(solvedList.get(row).get(0).getVal());
            }

        }

        double endTime = System.nanoTime()/1000000;
        double elapsed = endTime-beginTime;
        System.out.println("Tempo totale composizione puzzle: "+ elapsed+"ms");

    }

    public String getLinearString() {
        return linearString.toString();
    }

    public String getMatrixString() {
        return matrixString.toString();
    }

    //Ritorna il puzzle risolto, reso immutabile
    public List<List<PuzzlePiece>> getSolvedList() {
        return Collections.unmodifiableList(solvedList);
    }

}
