import io.FileManager;
import puzzle.CharPuzzleParser;
import puzzle.CharPuzzlePiece;
import puzzle.PuzzleBuilder;
import puzzle.PuzzleParser;


public class PuzzleSolver {

    public static void main(String[] args) {

        //Lettura percorsi del file di input e di output
        String inputPath = args[0];
        String outputPath = args[1];

        //Lettura da file a stringa
        String inputContent = FileManager.readContent(inputPath);

        if (!inputContent.isEmpty()) {

            //Parsing della stringa
            PuzzleParser parser = new CharPuzzleParser(inputContent);


            //Creo l'output finale
            PuzzleBuilder builder =
                    new PuzzleBuilder(parser.getPuzzleMap(), parser.getAngle(), CharPuzzlePiece.emptyID);

            int rowCount = builder.getSolvedList().size(),
                colCount = parser.getPuzzleMap().size()/rowCount;

            String outputString =
                    builder.getLinearString() + FileManager.newLine + FileManager.newLine + builder.getMatrixString() + FileManager.newLine + rowCount + ' ' + colCount;

            //Scrittura output su file
            FileManager.writeContent(outputPath, outputString);

        } else {

            //Se il file di input era vuoto, nel file di output scrive solo la dimensione del puzzle (0x0)
            FileManager.writeContent(outputPath, FileManager.newLine + FileManager.newLine + FileManager.newLine + FileManager.newLine + "0 0");//Le 4 FileManager.newLine servono per mantenere l'output coerente con i requisiti
        }
    }

}